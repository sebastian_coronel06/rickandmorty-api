const botonActualizar = document.getElementById('botonActualizar')

//capturando los elementos html mediante su 'id'
const imagenPersonaje = document.getElementById('imagenPersonaje')
const nombrePersonaje = document.getElementById('nombrePersonaje')
const estadoPersonaje = document.getElementById('estadoPersonaje')
const especiePersonaje = document.getElementById('especiePersonaje')
const generoPersonaje = document.getElementById('generoPersonaje')
const tipoPersonaje = document.getElementById('tipoPersonaje')
const idPersonaje = document.getElementById('idPersonaje')

//boton actualizar
botonActualizar.addEventListener('click', () => {
    //location.reload()
    traerPersonaje(random(1,50))
    console.log(random(1,50))
})

// res == respuesta == response
const traerPersonaje = (id) => {
    fetch('https://rickandmortyapi.com/api/character/'+ id) //consulta a la api, concatenando un 'id'
    .then(res => res.ok ? Promise.resolve(res) : Promise.reject(res))
        .then(res => res.json())
        .then(res => {
            const descripcion = document.getElementById('descripcion')
            const fragment = document.createDocumentFragment()
                const listItem = document.createElement('LI')
                idPersonaje.textContent = `id: ${res.id}`
                nombrePersonaje.textContent = `${res.name}`
                estadoPersonaje.textContent = `${res.status}`
                especiePersonaje.textContent = `${res.species}`
                generoPersonaje.textContent = `${res.gender}`
                tipoPersonaje.textContent = `${res.type}`
                imagenPersonaje.setAttribute("src", `${res.image}`)
                fragment.appendChild(listItem)
            //descripcion.appendChild(fragment)
        })
}

//Recargar el personaje utilizando la letra R en el teclado
window.addEventListener('keyup', (e) => {
    if(e.key == 'r') {
        traerPersonaje(random(1,50))
        console.log(random(1,50))
    }
    //console.log(e.key)
})
//Obtener un número aleatorio para el "id" de los personajes
const random = (min, max) => {
    return Math.floor((Math.random() * (max - min + 1)) + min);
}
